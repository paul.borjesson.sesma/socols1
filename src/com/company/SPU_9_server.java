package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SPU_9_server {

    private static final int port = 9090;
    private static boolean end = false;
    private static boolean veureMenu = true;

    public static void main(String[] args) {
        System.out.println("Inici");
        escoltar();
    }

    private static void escoltar() {
        ServerSocket serverSocket;
        Socket clientSocket;

        try {
            serverSocket = new ServerSocket(port);

            while (!end) {
                clientSocket = serverSocket.accept();

                processarComunicacionsAmbClient(clientSocket);

                tancarClient(clientSocket);
                System.out.println("El client s'ha desconectat, esperant a un altre...");
            }

            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void tancarClient(Socket clientSocket) {
        if (clientSocket != null && !clientSocket.isClosed()) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownInput();
                }
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void processarComunicacionsAmbClient(Socket clientSocket) {
        BufferedReader entradaClientSocket;
        PrintWriter sortidaCapAClientSocket;
        String missatgeClient;
        Boolean endComms;

        try {
            entradaClientSocket = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            sortidaCapAClientSocket = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);
            do {
                missatgeClient = entradaClientSocket.readLine();

                endComms = procesarMissatgesDelClient(clientSocket, missatgeClient, entradaClientSocket, sortidaCapAClientSocket);
            } while (!endComms);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean procesarMissatgesDelClient(Socket clientSocket, String missatgeDelClient, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        String tipusMissatge = "";
        StringTokenizer st;
        boolean acabarConnexioAmbElClient = false;

        // El client és qui inicialitza la comunicació i per tant enviarà un 'DEMANAR_PERMIS_CONNEXIO'.

        // Aquest codi és per quan el client talla la comunicació, al cridar tancarSocket() s'envia
        // automàticament un NULL pel socket que ens arriba aquí.
        // SEMBLA QUE QUAN S'ACABA D'ENVIAR UN FITXER AMB EL FTP TAMBÉ PASSA AIXÒ
        if (missatgeDelClient != null) {
            tipusMissatge = missatgeDelClient;
        } else {
            tipusMissatge = "tipus";
            acabarConnexioAmbElClient = true;
        }

        veureMenu = false;

        if (tipusMissatge.equals("DEMANAR_PERMIS_CONNEXIO")){
            respondreAPermisConnexio(sortidaCapAlClientSocket_en_caracters);
        }

        if (tipusMissatge.equals("RETORN_CONTROL")){
            veureMenu = true;
        }

        if (tipusMissatge.equals("INICIALITZAR_CHAT")){
            chat(false, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
            System.out.println();
        }

        if (veureMenu) {
            do {
                menu.delete(0, menu.length());

                menu.append(System.getProperty("line.separator"));
                menu.append("SPU-9 SERVER ");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("0. Desconnectar-se del CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));
                menu.append("1. Retornar el control de les comunicacions al CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append("2. CHAT");
                menu.append(System.getProperty("line.separator"));
                menu.append("3. FTP");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("50. Tancar el sistema");
                menu.append(System.getProperty("line.separator"));

                System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

                opcio = sc.next();

                switch (opcio) {
                    case "0":
                    case "50":
                        acabarConnexioAmbElClient = true;
                        veureMenu = false;
                        break;
                    case "1":
                        retornControl(sortidaCapAlClientSocket_en_caracters);
                        veureMenu = false;
                        break;
                    case "2":
                        chat(true, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
                        System.out.println();
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((!opcio.equals("50")) && (veureMenu) && (!acabarConnexioAmbElClient));
        }

        return acabarConnexioAmbElClient;
    }

    private static void respondreAPermisConnexio(PrintWriter sortidaCapAlClientSocket_en_caracters) {
        Scanner sc = new Scanner(System.in);
        String resposta;


        System.out.print("Permets la connexió del client? (S/N) ");
        resposta = sc.next();

        if (resposta.equalsIgnoreCase("S")) {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        sortidaCapAlClientSocket_en_caracters.flush();
    }



    private static void chat(boolean inicialitzemOperacio, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String missatgeAEnviar = "";
        String missatgeRebut = "";
        Scanner sc2 = new Scanner(System.in);
        boolean acabarChat = false;


        // El server inicialitza el chat.
        if (inicialitzemOperacio) {
            sortidaCapAlClientSocket_en_caracters.println("INICIALITZAR_CHAT");
            sortidaCapAlClientSocket_en_caracters.flush();

            try {
                do {
                    missatgeRebut = entradaPelClientSocket_en_caracters.readLine();
                    System.out.println("SERVER.chatejar(): missatge rebut del client: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        acabarChat = true;
                    } else {
                        System.out.print("SERVER.chatejar(): quin missatge vols enviar al client?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlClientSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlClientSocket_en_caracters.flush();
                    }

                } while (acabarChat == false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // El server NO inicialitza el chat.
        if (!inicialitzemOperacio) {
            retornControl(sortidaCapAlClientSocket_en_caracters);		// Retornem al client perquè pugui començar el chat.

            try {
                do {
                    missatgeRebut = entradaPelClientSocket_en_caracters.readLine();
                    System.out.println("SERVER.chatejar(): missatge rebut del client: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(sortidaCapAlClientSocket_en_caracters);		// Retornem al client el control de les comunicacions.

                        acabarChat = true;
                    } else {
                        System.out.print("SERVER.chatejar(): quin missatge vols enviar al client?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlClientSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlClientSocket_en_caracters.flush();
                    }
                } while (acabarChat == false);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void retornControl(PrintWriter sortidaCapAlSocket_en_caracters) {
        sortidaCapAlSocket_en_caracters.println("RETORN_CONTROL");
        sortidaCapAlSocket_en_caracters.flush();
    }

}
