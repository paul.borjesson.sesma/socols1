package com.company;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SPU_9_client {
    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 9090;
    private static final String separador = "$##$";
    private static boolean veureMenu = true;

    public static void main(String[] args) {
        System.out.println("SPU-9 Client - INICI");

        connectarAmbElServer(ADDRESS, PORT);

        System.out.println("SPU-9 Client - FI");
    }

    private static void connectarAmbElServer(String address, int port) {
        Socket socket;
        BufferedReader entradaPelSocket_en_caracters;
        PrintWriter sortidaCapAlSocket_en_caracters = null;
        String missatgeDelServer = "";
        boolean acabarComunicacio = false;

        // El client és qui inicialitza la comunicació.

        try {
            socket = new Socket(InetAddress.getByName(address), port);
            entradaPelSocket_en_caracters = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            sortidaCapAlSocket_en_caracters = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

            // Enviament de la petició de connexió. Inicialitzant la comunicació amb el
            // server.
            System.out.println("Client: demanem permís al server per a connectar-nos.");
            sortidaCapAlSocket_en_caracters.println("DEMANAR_PERMIS_CONNEXIO"); // Assegurem que acaba amb un final de
            // línia.
            sortidaCapAlSocket_en_caracters.flush();

            missatgeDelServer = entradaPelSocket_en_caracters.readLine();

            // Rebem si tenim o no tenim permís per a connectar-nos. El permís l'envia el
            // server.
            if (missatgeDelServer.equals("PERMIS_CONNEXIO_CONCEDIT")) {
                System.out.println("Client: tenim permís del server per a connectar-nos.");

                do {
                    acabarComunicacio = comunicarseAmbElServer(socket, entradaPelSocket_en_caracters,
                            sortidaCapAlSocket_en_caracters);
                } while (acabarComunicacio == false);

            } else {
                System.out.println("Client: NO tenim permís del server per a connectar-nos. Tanquem el socket.");
            }

            tancarSocket(socket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean comunicarseAmbElServer(Socket socket, BufferedReader in, PrintWriter out) {
        Scanner sc = new Scanner(System.in);

        boolean acabarConnexioAmbElServer = false;
        String serverMessage = "";
        StringBuilder menu = new StringBuilder("");
        String tipusMissatge = "";
        StringTokenizer st;

        if (veureMenu == false) {
            try {
                serverMessage = in.readLine();

                // Aquest codi és per quan el server talla la comunicació, al cridar tancarSocket() s'envia
                // automàticament un NULL pel socket que ens arriba aquí
                if (serverMessage != null) {
                    System.out.println("CLIENT.procesarMissatgeDelClient(): rebut del server el missatge: '" + serverMessage + "'.");
                    tipusMissatge = serverMessage;

                    if (tipusMissatge.equals("RETORN_CONTROL")) {
                        veureMenu = true;
                    }
                } else {
                    tipusMissatge = "patata";
                    veureMenu = false;
                    acabarConnexioAmbElServer = true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int opcio;
        if (veureMenu == true) {
            do {
                System.out.println(showMenu());
                System.out.println("Que vols fer? ");
                opcio = sc.nextInt();
                switch (opcio) {
                    case 0:

                    case 50:
                        acabarConnexioAmbElServer = true;
                        break;

                    case 1:
                        retornControl(out);
                        veureMenu = false;
                        break;

                    case 2:
                        chat(true, in, out);
                        System.out.println();
                        break;
                }
            } while ((opcio != 50) && (veureMenu) && (acabarConnexioAmbElServer == false));
        }

        return acabarConnexioAmbElServer;
    }

    private static void retornControl(PrintWriter out) {
        out.println("RETORN_CONTROL");
        out.flush();
    }

    private static void chat(boolean inicialitzemOperacio, BufferedReader in, PrintWriter out) {
        Scanner sc = new Scanner(System.in);

        String missatgeAEnviar = "";
        String missatgeRebut = "";
        boolean acabarChat = false;

        // El client inicialitza el chat
        if (true) {
            out.println("INICIALITZAR_CHAT");
            out.flush();

            try {
                do {
                    missatgeRebut = in.readLine();
                    System.out.println("CLIENT.chatejar(): missatge rebut del server: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        acabarChat = true;
                    } else {
                        System.out.print("CLIENT.chatejar(): quin missatge vols enviar al server?: ");
                        missatgeAEnviar = sc.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        out.println(missatgeAEnviar);
                        out.flush();
                    }
                } while (acabarChat == false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // El client NO inicialitza el chat
        if (!true) {
            retornControl(out); // Retornem al server perquè pugui començar el chat

            try {
                do {
                    missatgeRebut = in.readLine();
                    System.out.println("CLIENT.chatejar(): missatge rebut del server: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(out); // Retornem al server el control de les comunicacions

                        acabarChat = true;
                    } else {
                        System.out.print("CLIENT.chatejar(): quin missatge vols enviar al server?: ");
                        missatgeAEnviar = sc.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        out.println(missatgeAEnviar);
                        out.flush();
                    }
                } while (acabarChat == false);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del SERVER");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al SERVER");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

    private static void tancarSocket(Socket clientSocket) {
        // Si falla el tancament no podem fer gaire cosa, només enregistrar el problema
        // Tancament de tots els recursos
        if (clientSocket != null && !clientSocket.isClosed()) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
